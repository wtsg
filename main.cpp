#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glfw.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

#define NUM_TEXTURES 1
const char * tex_name[ NUM_TEXTURES ] = {
    "particle.tga",
};
GLuint tex_id[ NUM_TEXTURES ];
static void LoadTextures( void )
{
    int i;

    // Generate texture objects
    glGenTextures( NUM_TEXTURES, tex_id );

    // Load textures
    for( i = 0; i < NUM_TEXTURES; i ++ )
    {
        // Select texture object
        glBindTexture( GL_TEXTURE_2D, tex_id[ i ] );

        // Set texture parameters
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

        // Upload texture from file to texture memory
        if (!glfwLoadTexture2D( tex_name[ i ], 0 )) {
        	printf("Error loading %s\n",tex_name[i]);
        }
    }
}


extern "C" {
	#include "src/allocator.h"
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>

    extern int luaopen_glfw( lua_State *L );
    extern int luaopen_opengl (lua_State *L);

	static int draw_getFrameSize(lua_State *L) {
		int width,height;
		glfwGetWindowSize( &width, &height );
		lua_pushnumber(L,width);
		lua_pushnumber(L,height);
		return 2;
	}

	static int draw_glViewport (lua_State *L) {
		glViewport( luaL_checkint(L,1), luaL_checkint(L,2), luaL_checkint(L,3), luaL_checkint(L,4));
		return 0;
	}
	static int hasfield (lua_State *L,int idx, const char *name) {
		int res;
		lua_getfield(L,idx,name);
		res = lua_isnil(L,-1);
		lua_pop(L,1);
		return res;
	}
	static int draw_glClearColor (lua_State *L) {
		int bits = GL_COLOR_BUFFER_BIT;
		glClearColor( (float)luaL_checknumber(L,1), (float)luaL_checknumber(L,2), (float)luaL_checknumber(L,3), (float)luaL_checknumber(L,4));
		if (lua_istable(L,5)) {
			bits = (hasfield(L,5,"color")?GL_COLOR_BUFFER_BIT:0)|
				(hasfield(L,5,"depth")?GL_DEPTH_BUFFER_BIT:0);
		}
		glClear( bits );
		return 0;
	}

	static int draw_glRect (lua_State *L) {
		double x = luaL_checknumber(L,1);
		double y = luaL_checknumber(L,2);
		double w = luaL_checknumber(L,3);
		double h = luaL_checknumber(L,4);
		double r = luaL_optnumber(L,5,1);
		double g = luaL_optnumber(L,6,1);
		double b = luaL_optnumber(L,7,1);
		double a = luaL_optnumber(L,8,1);
		glColor4d(r,g,b,a);
		glBegin( GL_QUADS );
			glTexCoord2f( 0.0f, 0.0f );
			glVertex2d(x,y);

			glTexCoord2f( 1.0f, 0.0f );
			glVertex2d(x+w,y);

			glTexCoord2f( 1.0f, 1.0f );
			glVertex2d(x+w,y+h);

			glTexCoord2f( 0.0f, 1.0f );
            glVertex2d(x,y+h);
		glEnd();
		return 0;
	}

	static int framefunc (lua_State *L) {
		static int frame = 0;
		frame += 1;
		glRotatef(frame*.01f, 0.25f, 1.0f, 0.75f);
        glBegin( GL_TRIANGLES );
          glColor3f(0.1f, 0.0f, 0.0f );
          glVertex3f(0.0f, 3.0f, -4.0f);
          glColor3f(0.0f, 1.0f, 0.0f );
          glVertex3f(3.0f, -2.0f, -4.0f);
          glColor3f(0.0f, 0.0f, 1.0f );
          glVertex3f(-3.0f, -2.0f, -4.0f);
        glEnd();
        glBegin( GL_TRIANGLES );
          glColor3f(0.0f, 0.1f, 0.0f );
          glVertex3f(0.0f, 3.0f, -3.0f);
          glColor3f(0.0f, 0.0f, 1.0f );
          glVertex3f(3.0f, -2.0f, -2.0f);
          glColor3f(1.0f, 0.0f, 0.0f );
          glVertex3f(-3.0f, -2.0f, 2.0f);
        glEnd();

		return 0;
	}

	static int traceback (lua_State *L) {
		if (!lua_isstring(L, 1))  /* 'message' not a string? */
			return 1;  /* keep it intact */
		lua_getfield(L, LUA_GLOBALSINDEX, "debug");
		if (!lua_istable(L, -1)) {
			lua_pop(L, 1);
			return 1;
		}
		lua_getfield(L, -1, "traceback");
		if (!lua_isfunction(L, -1)) {
			lua_pop(L, 2);
			return 1;
		}
		lua_pushvalue(L, 1);  /* pass error message */
		lua_pushinteger(L, 2);  /* skip this function and traceback */
		lua_call(L, 2, 1);  /* call debug.traceback */
		return 1;
	}


	static int docall (lua_State *L, int narg, int clear) {
		int status;
		int base = lua_gettop(L) - narg;  /* function index */
		lua_pushcfunction(L, traceback);  /* push traceback function */
		lua_insert(L, base);  /* put it under chunk and args */
		status = lua_pcall(L, narg, (clear ? 0 : LUA_MULTRET), base);
		lua_remove(L, base);  /* remove traceback function */
		/* force a complete garbage collection in case of errors */
		if (status != 0) lua_gc(L, LUA_GCCOLLECT, 0);
		return status;
	}

	static int getmemstat (lua_State *L) {
		MemManager *m;
		int i;
		lua_pushvalue(L,lua_upvalueindex(1));
		m = (MemManager *) lua_touserdata(L,-1);
		lua_createtable(L,MEMMANAGER_BUCKETCOUNT*5,0);
		for (i=0;i<MEMMANAGER_BUCKETCOUNT; i+=1) {
			MemBucket *b = &m->buckets[i];
			lua_pushnumber(L,b->statTotalFrees);
			lua_pushnumber(L,b->statTotalAllocs);
			lua_pushnumber(L,b->statAllocs);
			lua_pushnumber(L,b->statListSize);
			lua_pushnumber(L,b->elementSize);
			lua_rawseti(L,-6,i*5+1);
			lua_rawseti(L,-5,i*5+2);
			lua_rawseti(L,-4,i*5+3);
			lua_rawseti(L,-3,i*5+4);
			lua_rawseti(L,-2,i*5+5);
		}
		return 1;
	}

}


int main()
{
    int     width, height;
    int     frame = 0;
    bool    running = true;

    MemManager *m = MemManager_create();

	lua_State *L;

	L = lua_newstate((lua_Alloc)MemManager_allocate,(void*)m);
	//L = luaL_newstate();

    glfwInit();

	if( !glfwOpenWindow( 512, 512, 0, 0, 0, 0, 0, 0, GLFW_WINDOW ) )
    {
        glfwTerminate();
        return 0;
    }
	luaL_openlibs(L);

	static const luaL_reg drawlib[] = {
		{"getFrameSize",draw_getFrameSize},
		{"glViewport",draw_glViewport},
		{"rect",draw_glRect},
		{"framefunc",framefunc},
		{NULL,NULL}
	};

	luaL_register(L, "draw", drawlib);

    luaopen_opengl(L);
    luaopen_glfw(L);

	lua_newtable(L);

	lua_pushlightuserdata(L,m);
	lua_pushcclosure(L,getmemstat,1);
	lua_setfield(L,-2,"getMemStat");

	lua_setglobal(L,"engine");
	if (luaL_loadfile(L,"main.lua")) {
		printf("Syntax error: %s\n",lua_tostring(L,-1));
		lua_pop(L,1);
	}
	else {
		if (docall(L,0,1)) {
			printf("LUA SCRIPT ERROR:\n%s",lua_tostring(L,-1));
			lua_pop(L,1);
		}
	}

    glfwSetWindowTitle("GLFW Application");
	LoadTextures();

	int errorfree = 1;
	double t = glfwGetTime();
    while(running)
    {
        // exit if ESC was pressed or window was closed
        running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam( GLFW_OPENED);
		double t2 = glfwGetTime();
		if (t2-t>1) {
			int fps = frame;
			char out[20];
			sprintf(out,"fps: %i",fps);
			glfwSetWindowTitle(out);
			frame = 0;
			t = t2;
		}
        frame++;

        glfwGetWindowSize( &width, &height );

        if (errorfree) {
			height = height > 0 ? height : 1;

			glViewport( 0, 0, width, height );

			glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
			glClear( GL_COLOR_BUFFER_BIT );

			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			//gluPerspective( 65.0f, (GLfloat)width/(GLfloat)height, 1.0f, 100.0f );
			gluOrtho2D(0,width,0,height);

            //glFrustum (0, 1, 0, .1, 0, 100);
            glScaled(1,1,.001);

			// Draw some rotating garbage
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();

			glEnable( GL_TEXTURE_2D );
			glEnable( GL_BLEND );
			//glEnable( GL_COLOR_MATERIAL );
			glBlendFunc( GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA );
			glBindTexture( GL_TEXTURE_2D, tex_id[ 0 ] );

			/*gluLookAt(0.0f, 0.0f, -10.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f );*/

			lua_settop(L,0);
			lua_getglobal(L,"engine");
			if (lua_type(L,-1) == LUA_TTABLE) {
				lua_getfield(L,-1,"frame");
				if (lua_type(L,-1) == LUA_TFUNCTION) {
					if (docall(L,0,1)) {
						printf("LUA SCRIPT ERROR:\n%s",lua_tostring(L,-1));
						lua_pop(L,1);
						errorfree = 0;
					}
				}
			}
			//glTranslatef( 1.0f, 1.0f, 0.0f );

        }
		glfwSwapBuffers();

    }

    glfwTerminate();

    return 0;
}
