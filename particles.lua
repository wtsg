
particle = {list = {}}
function particle:spawn (x,y,vx,vy,maxage)
	local p = {x=x,y=y,vx=vx,vy=vy,age=0,maxage = maxage or 10}
	self.list[#self.list+1] = p
	return p
end
local lut = {{0,0,0,0.3,5,t=0},{1,1,1,0.2,10,t=0.2},{0,0,1,0,50,t=1}}
table.sort(lut,function(a,b) return a.t<b.t end)


local simtime = glfw.GetTime()
local updateinterval = .01
function particle:step(catchup)
    catchup = catchup or 0
    local now = glfw.GetTime()
    if simtime+updateinterval>now then return end
    simtime = simtime + updateinterval

	if math.random()>.9 then
		local p = particle:spawn(200,200,(math.random()-.5),(math.random()-.4),math.random(1000,1800))
		function p:think()
			p.vx,p.vy = p.vx*.99,p.vy*.99-.002
			local m,a = self.maxage,self.age
			local p = a/m

			local mincol,maxcol = lut[1],lut[#lut]
			for i=1,#lut do
                if lut[i].t > mincol.t and lut[i].t < p then mincol = lut[i] end
                if lut[i].t >= p then maxcol = lut[i] break end
			end
			local p = (p - mincol.t) / (maxcol.t - mincol.t)
			local r1,g1,b1,a1,s1 = unpack(mincol)
			local r2,g2,b2,a2,s2 = unpack(maxcol)
			local q = 1-p
			self.r,self.g,self.b,self.a,self.s = r1*q+r2*p, g1*q+g2*p, b1*q+b2*p, a1*q+a2*p, s1*q+s2*p

			if self.y < 120 then
                self.vy = 0
                self.vx = math.max (-0.2, math.min (0.2, self.vx * 1.03))
            end
		end
		function p:draw()
			local x,y = self.x,self.y
			local m,a = self.maxage,self.age
			local p = 1-a/m
			local s = self.s
            gl.Enable "COLOR"
			draw.rect(x-s, y-s, s*2, s*2, self.r,self.g,self.b,self.a)
		end
	end


	for i=#self.list,1,-1 do
		local p = self.list[i]
		p.x = p.x + p.vx
		p.y = p.y + p.vy
		p.age = p.age + 1
		if p.think then p:think() end
		if p.age > p.maxage then
			table.remove(self.list,i)
		end
	end
	local now = glfw.GetTime()
    if simtime+updateinterval<now then
        return self:step(catchup + 1)
    elseif catchup>1 then
        --print("catched up",catchup)
    end
end
function particle:draw()
	local rect = draw.rect
	for i=#self.list,1,-1 do
		local p = self.list[i]
		if p.draw then p:draw() else
			rect(p.x-2, p.y-2, 4, 4, 1, 1, 1, 1)
		end
	end
end
