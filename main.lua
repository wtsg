require "lfs"

require "glfw"
print (glfw)

print "lua is up and running"
print (gl)

for k,v in pairs (gl) do
    print (k,v)
end

print (("="):rep(30))

for k,v in pairs (glfw) do
    print (k,v)
end

do
    local last_mtime
    function reload (enforce)
        if not enforce then
            local mtime = lfs.attributes ("particles.lua", "modified")
            if last_mtime == mtime then
                return
            end
            last_mtime = mtime
        end
        xpcall(function()
                dofile "particles.lua"
                print "particles reloaded!"
                errorfree = true
            end,
            function (err)
                print(debug.traceback("failed loading: "..err))
            end)
    end
end

function keyfun( key, action )
  if action == glfw.RELEASE then
    return
  end

  if key == glfw.KEY_ESC then
    running = false
  end
    if key == glfw.KEY_F5 then
        reload (true)
    end
end

glfw.SetKeyCallback( "keyfun" );

dofile "particles.lua"
errorfree = true

frame = 0
function engine.frame ()
    frame = frame + 1
    if frame %1000 == 0 then
		local info = engine.getMemStat()
		print "MEMORY STATS"
		for i=1,#info,5 do
			local t = {}
			for j=i,i+4 do
				t[j-i+1] = info[j]
			end
			if t[4]>0 then
				print("Stat:",unpack(t))
			end
		end
    end
	draw.rect(0,0,3,3,1,1,1,1)
	draw.rect(5,0,1,5,1,0,0,1)
	draw.rect(0,5,5,1,0,1,1,1)

    gl.Disable "TEXTURE_2D"
    gl.Translate(250,0,0)
   -- gl.Rotate(frame*.1,0,1,0)
    gl.Translate(-250,0,0)

    gl.PushMatrix()
    gl.Translate(50,150,0)
    gl.Scale(20,10,20)
    gl.Rotate(45,0,0,-1)

    gl.Begin "QUADS"
        gl.Color(1,1,0,1)

        gl.TexCoord(0,0)
        gl.Vertex(-1,-1,0)

        gl.TexCoord(1,0)
        gl.Vertex(1,-1,0)

        gl.Color(1,1,0,0)

        gl.TexCoord(1,1)
        gl.Vertex(1,1,0)

        gl.TexCoord(0,1)
        gl.Vertex(-1,1,0)
    gl.End()

    gl.PopMatrix()

    gl.Enable "TEXTURE_2D"

    --print(gl.GetError())


	--io.write("\r",#particle.list)
    if errorfree then
        xpcall(
            function()
                particle:step()
                particle:draw()
            end,
            function (err)
                print(debug.traceback(err))
                errorfree = false
            end
        )
    end

    reload();

--	draw.framefunc()
--	draw.rect(0,0,10,10,1,1,0,1)

end
