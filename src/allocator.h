#ifndef __ALLOCATOR_H__
#define __ALLOCATOR_H__

typedef struct MemBlock_s {
    struct MemBlock_s *next;
} MemBlock;

typedef struct {
    size_t elementSize;
    MemBlock *firstFree;

    int statTotalAllocs;
    int statAllocs;
    int statTotalFrees;
    int statListSize;
} MemBucket;

#define MEMMANAGER_BUCKETCOUNT 28
#define MEMMANAGER_MINMEMSIZE 16

typedef struct {
    MemBucket buckets[MEMMANAGER_BUCKETCOUNT];
} MemManager;

/** creates a memorymanager object */
MemManager* MemManager_create ();
/** allocator that is Lua alloc conform */
void* MemManager_allocate (MemManager *self, void *ptr, size_t osize, size_t nsize);

void MemManager_free (MemManager *self);


#endif
