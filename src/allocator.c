#include "stdlib.h"
#include "memory.h"
#include "allocator.h"



MemManager* MemManager_create () {
    MemManager *self = (MemManager*) calloc(1,sizeof(MemManager));
    int i,s;
    for (i=0,s=MEMMANAGER_MINMEMSIZE; i<MEMMANAGER_BUCKETCOUNT; i+=1, s *= 2) {
        self->buckets[i].elementSize = s;
        self->buckets[i].firstFree = NULL;
        self->buckets[i].statAllocs = 0;
        self->buckets[i].statListSize = 0;
        self->buckets[i].statTotalAllocs = 0;
        self->buckets[i].statTotalFrees = 0;
	}
    return self;
}

static MemBucket* MemManager_getBucket (MemManager *self, size_t minsize) {
    // assumption: 4gb allocation size is enough
    int bucket = 0;
    // linear search is OK - just 28 steps at max and anything>16 is highly unlikely
    // - most allocations will be <256 bytes, so at average 3-4 calculations -
    // vs. 3-4 steps in worst case with binary search that is however much more complex
    while (self->buckets[bucket].elementSize < minsize) ++bucket;
    return &self->buckets[bucket];
}

void* MemManager_allocate (MemManager *self, void *ptr, size_t osize, size_t nsize) {
    MemBucket *obucket = NULL, *nbucket = NULL;
    void *nptr = NULL;
    if (osize>0) {
        obucket = MemManager_getBucket(self,osize);
    }
    if (nsize>0) {
        nbucket = MemManager_getBucket(self,nsize);
    }
    if (obucket == nbucket) {
        return ptr;
    }
    if (nbucket) {
        if (nbucket->firstFree) {
            nptr = (void*) nbucket->firstFree;
            nbucket->firstFree = nbucket->firstFree->next;
            nbucket->statListSize -= 1;
        }
        else {
            nptr = malloc(nbucket->elementSize);
            nbucket->statAllocs += 1;
        }

        nbucket->statTotalAllocs += 1;
    }
    if (obucket) {
        MemBlock *block = (MemBlock*) ptr;
        if (nbucket) {
            memcpy(nptr,ptr,osize>nsize?nsize:osize);
        }

        obucket->statTotalFrees += 1;
        obucket->statListSize += 1;

        block->next = obucket->firstFree;
        obucket->firstFree = block;
    }
    return nptr;
}


void MemManager_free (MemManager *self) {
    int i;
    for (i=0;i<MEMMANAGER_MINMEMSIZE;i+=1) {
        MemBlock *browse = self->buckets[i].firstFree;
        while (browse) {
            MemBlock *old = browse;
            browse = browse->next;
            free((void*)old);
        }
    }
    free(self);
}
